# Subdirectory: Data-collection-instruments

You can store and make public your data collection instruments, such as your survey, semi-structured interview guide, or focus group interview guide.


# Subdirectory: Data-collection-training

You may want to train other researchers in your research group to conduct data collection. This may involve several (iterations of) processes, to which the materials can be stored here and made public.