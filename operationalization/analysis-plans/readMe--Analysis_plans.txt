# Subdirectory: Mock-ups
Your analysis plans may be supplemented with figures, visualizations, illustrations that can be stored in this subdirectory. Some of these include mind maps, mock epistemic networks, mock graphs/charts of quantified qualitative data, etc.


# Open analysis tools:

The Reproducible Open Coding Kit (ROCK)
https://rock.science

jamovi
https://www.jamovi.org

Epistemic Network Analysis (ENA)
https://www.epistemicnetwork.org


# Tools for systematic reviews:

AS review
https://asreview.nl

Metabefor
https://gitlab.com/r-packages/metabefor