# Subdirectory: Data-anonymization
This can be a good place to keep instructions on and materials for data anonymization. See below for more information on anonymization.

# Subdirectory: Data-transcription
This subdirectory may house instructions on and materials for data transcription (e.g., from audio to text).


# Sharing data
If your participants did consent to making their data public, and you have successfully anonymized the data, you may want to share it via data repositories, such as:

The Qualitative Data Repository
https://qdr.syr.edu

FAIRsharing (standards, databases, policies)
https://fairsharing.org

Center for Open Science
https://www.cos.io

European Open Science Cloud
https://eosc-portal.eu

re3data (Registry of Open Access Repositories)
https://www.re3data.org

Zotero
https://www.zotero.org

