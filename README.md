# Introduction to Open Science practices and tools 
This is the readMe file of the repository housing materials for the Introduction to Open Science practices and tools PhD course.

## Basic Description of Course

### Background
Open Science (OS) is a movement aimed at transforming scientific practice in order to address “the changes, challenges, opportunities and risks of the 21st century digital era and to increase the societal impact of science in response to the growing and complex global issues facing humanity” (UNESCO 2019). OS can be seen as a novel paradigm for scientific initiatives, promoting transparency, sharing, and sustainability. By increasing transparency throughout the research process, OS inevitably spurs methodological rigor and verifiability via the possibility to scrutinize and openly evaluate processes of knowledge production. Open processes further science and improve the effectiveness of scientific systems e.g., by increasing reusability and reducing research waste. 


### Objectives and outcomes
The course aims to familiarize doctoral students with OS practices and tools to encourage their uptake during the PhD trajectory and beyond. By the end of the course, students should:
-	Understand the fundamental aspects of OS
-	Be able to preregister their research
-	Manage their project effectively and openly
-	Know how to upload preprints and select appropriate licenses for their scientific outputs
-	Use multiple pieces of open source software for data and citation management, as well as analysis
-	Be familiar with reporting standards and making research materials and processes open


### Course requirements
Students are required to attend each session of the course, complete a portfolio, and take an oral exam. The portfolio is a compilation of the activities we will perform during the course, requiring a bit of extra work to complete (see list below). The oral exam entails answering some questions about the tools and practices contained in the portfolio (e.g., basic use of tools, basic functionality of interfaces, advantages and challenges of their use) and about the mandatory readings (see syllabus).

### Overview 
#### Module 1 (Fri, 4x45min)
Overview of OS
Preregistration

#### Module 2 (Sat, 8x45min)
Project management
Data management
Public repositories
Open data and FAIR principles

#### Module 3 (Sun, 8x45min)
FLOSS tools
Citation management
Open Access
Preprints and archives

#### Module 4 (Mon, 4x45min)
Reporting standards
Open peer review

#### Module 5 (Tue, 4x45min)
Participatory methods
Open educational materials

### Portfolio contents

- Downloaded empty preregistration template of choice
- Downloaded example of a filled-in preregistration with the template above
- Data management plan with answers to the 3 questions we will discuss in class
- Active account at the Open Science Framework (provide URL to your account)
- Public project with (mock) content (Title, contributors, short description, DOI, license, tags, wiki, sample files: at least 3 directories, one subdirectory, and 3 files – these can be mock or actual; provide URL to your project)
- A (mock) preregistration uploaded to OSF Registries and linked to the OSF project
- Metadata file created in class
- Files coded with iROCK created in class
- Output created with jamovi in class
- A Word file containing at least 3 citations and a bibliography created with Zotero in class
- A screen shot of Zotero cloud with a shared library created in class
- A list of at least 3 Open Access journals that are appropriate for you to publish in and are sponsored by Semmelweis Library
- An active account with and a (mock) preprint uploaded to a preprint server of your choice (provide URL to your preprint)
- A (mock) poster uploaded to your OSF project created with a “Better Poster” template with a working QR code (that takes you to the OSF page displaying your poster)
- A (mock) PRISMA flow diagram created with the PRISMA flow diagram tool, exported to jpg or pdf
- A downloaded example of or URL to a published registered report (by other researchers)
- An example of a citizen science project or project involving participatory research that you find interesting/useful (provide URL)

